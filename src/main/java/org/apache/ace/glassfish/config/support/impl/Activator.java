package org.apache.ace.glassfish.config.support.impl;

import org.apache.ace.client.repository.helper.ArtifactHelper;
import org.apache.ace.client.repository.helper.ArtifactRecognizer;
import org.apache.ace.client.repository.helper.bundle.BundleHelper;
import org.apache.ace.client.repository.object.ArtifactObject;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

import java.util.Dictionary;
import java.util.Hashtable;

/**
 * @Author Paul Bakker - paul.bakker.nl@gmail.com
 */
public class Activator extends DependencyActivatorBase {
    @Override
    public void init(BundleContext bundleContext, DependencyManager manager) throws Exception {
        Dictionary<String, String> props = new Hashtable<String, String>();
        props.put(ArtifactObject.KEY_MIMETYPE, ConfigurationHelperImpl.MIMETYPE);
        ConfigurationHelperImpl helperImpl = new ConfigurationHelperImpl();
        manager.add(createComponent()
            .setInterface(ArtifactHelper.class.getName(), props)
            .setImplementation(helperImpl));
        manager.add(createComponent()
            .setInterface(ArtifactRecognizer.class.getName(), null)
            .setImplementation(helperImpl));
    }

    @Override
    public void destroy(BundleContext bundleContext, DependencyManager dependencyManager) throws Exception {
    }
}
