package org.apache.ace.glassfish.config.support.impl;

import org.apache.ace.client.repository.helper.ArtifactHelper;
import org.apache.ace.client.repository.helper.ArtifactPreprocessor;
import org.apache.ace.client.repository.helper.ArtifactRecognizer;
import org.apache.ace.client.repository.object.ArtifactObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @Author Paul Bakker - paul.bakker.nl@gmail.com
 */
public class ConfigurationHelperImpl implements ArtifactHelper, ArtifactRecognizer {
    public static final String MIMETYPE = "application/vnd.org.apache.ace.glassfish.configuration";
    public static final String PROCESSOR_PID = "org.apache.ace.glassfish.configuration.processor";
    public static final String KEY_FILENAME = "filename";

    public String recognize(URL artifact) {
        if (artifact.getFile().endsWith(".gf")) {
            return MIMETYPE;
        }

        return null;
    }

    public Map<String, String> extractMetaData(URL artifact) throws IllegalArgumentException {
        Map<String, String> result = new HashMap<String, String>();
        result.put(ArtifactObject.KEY_PROCESSOR_PID, PROCESSOR_PID);
        result.put(ArtifactObject.KEY_MIMETYPE, MIMETYPE);

        String name = new File(artifact.getFile()).getName();
        String key = KEY_FILENAME + "-";
        int idx = name.indexOf(key);
        if (idx > -1) {
            int endIdx = name.indexOf("-", idx + key.length());
            name = name.substring(idx + key.length(), (endIdx > -1) ? endIdx : (name.length() - getExtension(artifact).length()));
        }
        result.put(ArtifactObject.KEY_ARTIFACT_NAME, name);
        result.put(KEY_FILENAME, name);
        return result;
    }

    public boolean canHandle(String mimetype) {
        return mimetype.equals(MIMETYPE);
    }

    public String getExtension(URL artifact) {
        return ".gf";
    }

    public boolean canUse(ArtifactObject object) {
        return MIMETYPE.equals(object.getMimetype());
    }

    public ArtifactPreprocessor getPreprocessor() {
        return null;
    }

    public <TYPE extends ArtifactObject> String getAssociationFilter(TYPE obj, Map<String, String> properties) {
        return "(" + KEY_FILENAME + "=" + obj.getAttribute(KEY_FILENAME) + ")";
    }

    public <TYPE extends ArtifactObject> int getCardinality(TYPE obj, Map<String, String> properties) {
        return Integer.MAX_VALUE;
    }

    public Comparator<ArtifactObject> getComparator() {
        return new Comparator<ArtifactObject>() {
            public int compare(ArtifactObject one, ArtifactObject other) {
                try {
                    Properties p = new Properties();
                    p.load(new FileInputStream(one.getURL()));

                    Properties p2 = new Properties();
                    p2.load(new FileInputStream(other.getURL()));

                    if (p.getProperty("type").equals("jdbc-connection-pool") && p2.getProperty("type").equals("jdbc-resource")) {
                        return 1;
                    } else if (p2.getProperty("type").equals("jdbc-connection-pool") && p.getProperty("type").equals("jdbc-resource")) {
                        return -1;
                    } else {
                        return 0;
                    }

                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        };
    }

    public Map<String, String> checkAttributes(Map<String, String> attributes) {
        return attributes;
    }

    public String[] getDefiningKeys() {
        return new String[]{KEY_FILENAME};
    }

    public String[] getMandatoryAttributes() {
        return new String[]{KEY_FILENAME};
    }

}
